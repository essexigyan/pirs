'use strict';

$( function () {

	const Vue = require( 'vue' ),
		App = require( './components/App.vue' ),
		/* eslint-disable no-jquery/no-global-selector */
		$vueContainer = $( '<div>' ).addClass( 'pirs-special-page-container' );

	$( '#content' ).append( $vueContainer );
	Vue.config.compilerOptions.whitespace = 'condense';
	Vue.createMwApp( App )
		.mount( $vueContainer.get( 0 ) );

	//var tabs = OO.ui.infuse( $( '#pirs-tabs' ) );
} );
