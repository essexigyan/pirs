const Main = require('../../resources/components/App');
const utils = require("@vue/test-utils");

describe('Main Component Test Suite', () => {
	test('tests activeStep default is 1', () => {
		const wrapper = utils.shallowMount(Main)
		const vm = wrapper.vm

		expect(vm.activeStep).toBe(1)

			vm.goToStep(3)

		expect(vm.activeStep).toBe(3)
	});

	test('tests goToStep() method changes to 3', () => {
		const wrapper = utils.shallowMount(Main)
		const vm = wrapper.vm

		vm.goToStep(3)
		expect(vm.activeStep).toBe(3)
	})

});

