<?php

use MediaWiki\MediaWikiServices;
use OOUI\ButtonWidget;
use OOUI\IndexLayout;
use OOUI\TabPanelLayout;
use OOUI\Tag;
use MediaWiki\Session\SessionManager;
use MediaWiki\User\UserOptionsLookup;


/**
 * pirs SpecialPage for pirs extension
 *
 * @file
 * @ingroup Extensions
 */
class SpecialPIRS extends SpecialPage {

	public function __construct() {
		parent::__construct( 'pirs' );
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription() {
		return $this->msg( 'Pirs' )->text();
	}
	/**
	 * @var string
	 */
	protected $formType;

	protected function getPirsConfig() {
		$contactConfig = $this->getConfig()->get( 'PIRSConfig' );
		return $contactConfig['steps'];
	}


	public function execute( $par ) {

		$request = $this->getRequest();
		//$this->formType = strtolower( $request->getText( 'formtype', $par ?? '' ) );
		$config = $this->getPirsConfig();
		$this->getOutput()->enableOOUI();

		//var_dump($config);

		$user = $this->getUser();

		$this->getOutput()->setPageTitle(
			$this->msg( 'pirs-title' )
		);
//
//		$main = new IndexLayout( [
//			'infusable' => true,
//			'expanded' => false,
//			'framed' => false,
//			'id' => 'pirs-tabs'
//		] );
//
//		$foo = new OOUI\HtmlSnippet( 'STEP1');
//		$foo2 = new OOUI\HtmlSnippet( 'STEP2');
//		$foo3 = new OOUI\HtmlSnippet( 'STEP3');
//
//		$where = $this->getRequest()->getText('tab');
//
//
//		$tabs = [];
//		$tabs[] = $this->createTab('STEP1','S1',$foo);
//		$tabs[] = $this->createTab('STEP2','S2',$foo2);
//		$tabs[] = $this->createTab('STEP3','S3',$foo3);
//		$main->addTabPanels( $tabs );
//		$main->setTabPanel($where);
//
//
//
//
//		$this->getOutput()->addHTML($main);

	}

	public function processInput( $formData ) {

		print_r($formData);


	}

	protected function getGroupName() {
		return 'other';
	}

	private function createTab( string $name, string $label, \OOUI\HtmlSnippet $content ): TabPanelLayout {
		return new TabPanelLayout(
			$name,
			[
				'id' => $name,
				'classes'=>['incidentreporting-tabs'],
				'label' => $label,
				'expanded' => false,
//				'tabItemConfig' => [
//					'href' => $this->getFullTitle()->getLinkURL( [ 'tab' => $name ] )
//				],
				'content' => $content
			]
		);
	}

}
